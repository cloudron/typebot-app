[0.1.0]
 * Initial version for Typebot v2.16.0

[0.2.0]
* For this update the app needs to be reinstalled, since encryption secret length has changed
* Ensure encryption secret key has correct length

[0.3.0]
* Use upstream built artifacts from Dockerimages instead of building locally

[0.4.0]
* Update Typebot to 2.17.1
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v2.17.1)
* (dateInput) Add format option and improve parsing [9e8fa12]
* (wait) Add pause option on Wait block [111fb32]
* Make sure to add start client side action first in the list [1ebacaa]
* (openai) Add custom provider and custom models [27a5f4e]
* Fix pt-BR i18n loading [be0c619]
* Fix select text in buttons node drag [5092e14]
* (openai) Fix create credentials modal not displaying [e8eaac4]
* (fileUpload) Fix web bot file upload input skip option [968c5e3]

[0.5.0]
* Update Typebot to 2.17.2
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v2.17.2)
* Remove docker main tag building [a0df851]
* Fix docker deployment tagging [f14808d]

[0.6.0]
* Update Typebot to 2.18.0
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v2.18.0)
* WhatsApp integration has been greatly improved. Also the documentation to set it up as a self-hosters has more details.
* New Zemantic AI bock. It's the first ever block contributed by the community

[0.7.0]
* Update Typebot to 2.18.2
* Update base image to 4.2.0
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v2.19.0)
* Automatically parse markdown from variables in text bubbles [cfc5f64]
* (openai) Improve streaming bubble sequence and visual [49826d1]
* Upgrade sentry and improve its reliability [3e7b9b3]
* (condition) Don't show value in node content if operator is "set" or "empty" [224a08b]
* Remove sentry client monitoring in viewer [073654e]
* Better parsing of lists and code in streaming bubbles [877a58d]
* (openai) Improve streamed message lists CSS [b232a94]
* (openai) Replace openai-edge with openai and upgrade next [225dfed]
* (api) Auto start bot if starting with input [9e6a1f7]
* (videoBubble) Reparse variable video URL to correctly detect provider [a7b784b]
* (sendEmail) Rename username SMTP creds label to avoid confusion [42ae75c]

[1.0.0]
* Update Typebot to 2.19.3
* First stable package release
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v2.19.3)
* Add convenient script for migrating Stripe prices [11186d8]
* Improve getUsage accuracy in check cron job [1cc4ccf]
* (buttons) Trim items content when parsing reply for better consistency [621fcd5]
* pdate import contact to brevo script [be9daee]
* (billing) Automatic usage-based billing (#924) [797751b]
* Fixed pinch zooming mouse issue (with ctrl key) (#940) [2c15662]
* Freeze body overflow when opening a Popup embed (#937) [df3a17e]
* Fix send email in CI "React is not defined" [3e06d89]
* (results) Lower the max limit in getResults endpoint to avoid payload size error [885dcec]
* (billing) Fix chats pricing tiers incremental flat amou… [6b0c263]
* (webhook) Fix webhook response data key number parsing [1d0aab7]

[1.0.1]
* Update Typebot to 2.19.4
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v2.19.4)
* Fix can invite new members in workspace bool [53558dc]
* (numberInput) Fix input clearing out on dot or comma press [4b248d5]
* Fix popup blocked toast typo [1ff5881]
* (whatsapp) Improve whatsapp start log [c2a08c4]

[1.1.0]
* Update Typebot to 2.19.0
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v2.19.4)
* Add cache-control header on newly uploaded files [d1502f8]
* Move cache control header into the post policy [a855d85]
* Fix plan name typo [bdf9fae]
* Fix formatted message in input block when input is retried [a564181]
* Fix CORSRules content typo for S3 config [585e1d4]
* Fix number input validation with variables [7586eca]
* Fix group duplicate new title bug [2d1ce73]
* Fix checkAndReportChatsUsage script sending multiple emails at once [3f7f094]
* Fix manual deployment doc start script typo [a347a27]
* Fix graph flickering on high res displays (#959) [f1e3836]
* Add text link section in text bubble doc [b80bea1]
* Add webhook configuration tuto video [3e02436]
* Migrate to Tolgee (#976) [bed8b42]
* Fix type resolution for @typebot.io/react and nextjs [31b3fc3]

[1.1.1]
* Update Typebot to 2.19.1
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v2.19.1)
* Add response debug log for failing requests without errors [5298538]
* Show scrollbar on searchable items [7cf64a1]
* Auto scroll once picture choice images are fully loaded [1f19eb8]
* date) Upgrade date parser package [00265af]
* (wordpress) Revert to specific non breaking version for self-hosters [6c0f28b]
* Add maxWidth and maxHeight bubble them props [74dd169]
* Revert new authentication method for preview bot [06065c3]
* Add more video supports (#1023) [dd4de58]
* Fix multi choice checkbox UI on small screens [60829b4]
* (typebotLink) Make "current" option work like typebot links instead of jump [64418df]
* Introduce typebot v6 with events (#1013) [35300ea]
* (chatwoot) Unmount Typebot embed bubble when opening chatwoot [eed562b]
* Upgrade Sentry to mitigate security issue [b2b82c4]
* (editor) Fix AB test items not connectable [3a47a0f]
* (typebotLink) Fix variables merging with new values [e22bd7d]
* (wordpress) Fix version mismatch for self-hosters for Standard embed as well [eca6d20]
* Fix typebot parsing for legacy columnsWidth setting [8d56349]
* (fileUpload) Fix results file display if name contains comma [bd198a4]
* (js) Fix default theme values css variables [fd00b6f]
* (billing) Set invoicing behavior to "always invoice" to fix double payment issue [a1d7415]
* (textBubble) Fix variable parsing when starting or finishing by spaces [23625ad]
* (webhook) Fix legacy webhook {{state}} body parsing [63233eb]
* Fix theme background and font default selection [e9a10c0]
* Sort variables to parse to fix text bubble parsing issue [a38467e]
* (editor) Fix edge delete with undefined groupIndex [647afdb]
* (webhook) Fix webhook execution with default method [14a3716]
* (typebotLink) Fix link to first group with start event [9bb5591]
* (zapier) Fix execute webhook endpoint too strict on block type check [9eef166]
* (editor) Fix move block with outgoing edge [58b9e0b]
* Fix default initial items in TableList [b73ca7a]
* Fix typebot publishing endpoint events parsing [4b67f9e]
* (import) Fix import typebot files that does not have name field [aceba0a]
* Fix parsing issue with new events field on ongoing session states [db17a0f]
* Fix weird env parsing on Firefox making it crash [eaa9b81]

[1.1.2]
* Symlink nextjs cache directory

[1.2.0]
* Update Typebot to 2.20.0
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v2.20.0)
* Allow user to share a flow publicly and make it duplicatable [bb41226]
* (openai) Add create speech OpenAI action [1a44bf4]
* (wordpress) Add lib_version prop in shortcode [eeac493]
* (fileUpload) Properly encode commas from uploaded file urls [8d413f0]
* Increase builder request max size to 4MB [4666fd8]
* (redirect) Make sure the redirection is always done on top frame [6ce43ed]
* (billing) Improve past_due workspace checking webhook [0856c59]
* Remove VIEWER_URL_INTERNAL variable [73d2e16]
* (share) Fix duplicate folderId issue [8ce4e48]
* Fix default webhook body with multi inputs groups [880ded9]
* (pictureChoice) Fix pic choice multi select parsing [b7ee800]
* Update broken action-autotag package [7f914e9]
* Fix processTelemetry endpoint not reachable [30b09e5]
* (billing) Fix stripe webhook "invoice.paid" typo [5b0073b]
* (pictureChoice) Fix choice parsing too unrestrictive [542e632]
* (editor) Fix typebot update permission [8a07392]
* (chatwoot) Fix email prefill when Chatwoot contact already exist [94886ca]
* Fix typebot v7 breaking changes doc typo [1e64a73]

[1.2.1]
* Fix sourcing of env file

[1.2.2]
* Update Typebot to 2.21.0
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v2.21.0)
* (webhook) Add Orimon in long request whitelist [3bd07c6]
* Introducing The Forge (#1072) [5e019bb]
* Only send suspicious bot alert if risk level is below 100 [eedb714]
* Introducing Radar, fraud detection [4fdc1bf]
* (radar) Remove IP ban system [7ce1a4d]
* (webhook) Stringify inline code for better reliability [1160f58]
* Attempt to fix tolgee random crash due to provider not detected [a235a7a]
* Add disableRequestTimeout param for automation integrations [cee1373]
* (webhook) Improve header and query params parsing [3bbaf67]
* Add anthropic to long request whitelist [f4a0935]
* (bot) Use ky for queries in bot to improve reliability [a653646]
* (sheets) Use Google Drive picker and remove sensitive OAuth scope [deab1a1]
* Add dynamic timeout to bot engine api [957eaf3]
* Update main viewer domain to typebot.co [8819e9e]
* Update vercel.json to reflect new api path [e2abfc6]
* emove references to old s3 URLs [c2fc2df]
* (docs) Open community search docs results in same tab [61f7f67]
* docs) Fix docs title suffix [6246429]
* Fix crash on toast show up due to tolgee provider not defined [7804ae2]
* Revert resultId optional in startChat response [6e076e9]
* Add back runtimeOptions and fix popup width option [867e68c]
* Chatwoot widget open should not unmount bot if standard or popup [512bb09]
* (whatsapp) Fix WA preview not starting and accept audio and documents messages [780b4de]
* Fix invalid timeout for long running integrations [64fc59b]
* (results) Fix result modal answers order [2dec0b8]
* (chatNode) Fix default responseMapping item run [06b7f40]
* (stream) Fix target attribute being trimmed on message stream [bf626bb]
* Release new embed lib version and fix createId dep… [fbddddc]
* Fix next/image not loading in self-hosting [c373108]
* Fix change language not working in the editor [0b93c2b]
* (sheets) Init OAuth client inside a function to avoid potential conflict [7fcc4fb]
* ix invalid ending comma in API instructions [99c5aaf]
* Fix right click in bubble text editor selects the group [32b2bb6]
* Fix multiple item dragged issue [a43d047]
* (editor) Fix old typebot flash when changing the typebot [cb87a72]
* (radar) Add cumulative keywords auto ban [00f8bbc]
* (radar) Improve scam detection by analyzing the entire typebot [7e3edfc]

[1.2.3]
* Update Typebot to 2.21.1
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v2.21.1)
* (logs) Remove some logs from API response to avoid sensit… [b5fbba7]

[1.2.4]
* Update Typebot to 2.21.2
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v2.21.2)
* (webhook) Disable webhook timeout if CHAT_API_TIM… [e8b9ef4]

[1.2.5]
* Update Typebot to 2.21.3
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v2.21.3)
* Muting Vercel bot notifications for viewer [5266be1]
* (webhook) Add custom timeout option [34917b0]
* (webhook) Fix test request execution invalid timeout [f73bc46]
* (webhook) Fix result parsing with same linked typebot [d247e02]

[1.3.0]
* Update Typebot to 2.22.0
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v2.22.0)

[1.4.0]
* Email display name support
* Optional SSO support

[1.4.1]
* Update Typebot to 2.22.1
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v2.22.1)
* Add posthog keys to github secrets [9f0c6b3]
* Track custom domain and WA enabled events [b9183f9]
* Send onboarding replies to PostHog [fd4867f]
* (fileUpload) Add success labels cutomization [9fc05cb]
* e-organize telemetry package [92a1d67]
* Revert to got for user created webhook to fix function crash [9014c4a]
* Fix user creation crashing [9222da6]
* Fix mockedUser default props [4122d73]
* Make sure variables are parsed in date and picture choice options [ce79e89]
* (openai) Fix 400 error when tools array empty [c616117]
* Fix inline variable parsing on new line issue [b660611]
* (buttons) Make sure to parse options if button has dynamic items [be5482c]
* Fix broken skippable file upload input [3128ebd]
* Fix new markdown parser on web runtime [678e6d5]
* Move ky package to typebot.io/lib [3e8e882]

[1.4.2]
* Update Typebot to 2.22.2
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v2.22.2)
* Fix key component in tooltip for dark mode [5e9dfaa]
* (folders) Make sure folders are not all listed in root folder [fa2eeff]
* (calCom) Make sure Cal.com book event is emitted once per block [83231e6]
* (folders) Make sure to exit folder on workspace change [c2603d9]

[1.5.0]
* Update Typebot to 2.23.0
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v2.23.0)

[1.5.1]
* Update Typebot to 2.24.0
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v2.24.0)

[1.5.2]
* Update Typebot to 2.24.1
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v2.24.1)
* (Revert stream condition for self-hosting) [7237901]
* (editor) Fix text bubble update conflict when deleting group [7ce6d73]

[1.5.3]
* Update Typebot to 2.25.0
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v2.25.0)
* (templates) New "Hight ticket follow up" template [a45e8ec]
* (theme) Add container theme options: border, shadow, filter (#1436) [5c3c7c2]
* Improve http req timeout error [75dd554]
* Improve toast error when whatsapp token is not valid [3b1b464]
* Add prisma metrics to prometheus endpoint (#1420) [6e0388c]
* mprove thread id saving conditions [40a10c7]
* Fix auto scroll behavior [0a7d598]
* Improve auto scroll behavior [a7fc413]
* Show send icon by default on textboxes [873ba0b]
* (blog) Fix static viewer rewrites for blog posts [fadcd3a]
* (blog) Fix viewer rewrites for blog styles [ccc974f]
* Add blog endpoint to viewer rewrites [e4e724d]
* Improve auto scroll behavior [5aad10e]
* Remove empty strings from variable parsing when possible [3ca1a2f]
* (whatsapp) Avoid multiple replies to be sent concurently [7bec58e]
* Improve zapier, make.com block content feedback [75cd141]
* (phoneInput) Add missing Dominican Republic dial codes [d608a30]
* (wordpress) Add the lib_version attribute to wp admin panel [f550870]
* (payment) Improve payment default currency be… [6594c56]
* igrate from got to ky (#1416) [d96f384]
* (dify) Only save Conversation ID when not emp… [ccc7101]
* Fix image bubble distortion on Safari [2f84b10]
* Remove nested prettierignore files [7aad60c]
* (dify) Auto convert non-md links to md flavoured links [68ad0f2]
* On chat state recover, don't execute client side actions [3aee9e7]
* Add embed lib auto patch script [69446ad]

[1.5.4]
* Update Typebot to 2.25.1
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v2.25.1)
* Add variables panel [1afa25a]
* (customDomain) Add debug log for custom domains req… [6f0e236]
* Add "OpenAI Assistant Chat" template [a413d1b]
* Add "Quick Carb Calculator" template [04e29ba]
* templates) Update variables isSession props [218f689]
* (billing) Increase invoices limit list [e4a7774]
* (blog) Revert rewrite referer regex and manually add entry for /blog [58ba6a4]
* (elevenlabs) Remove variable button in cred… [8a27cea]
* (variables) Add session option in variables (#1490) [b4ae098]
* Fix inconsistent updatedAt when timezone is different fro… [ad4d1a1]
* (blog) Fix referer regex matcher [1e5e085]
* (blog) Replace matching host regex instead of multi value in list [29d0cae]
* (blog) Fix image not loading when coming from /blog [30f81c8]
* (payment) Fix description variable parsing [2578335]
* (share) Show duplicate button for authenticated guests [9b1ff84]
* Fix webhook default timeout and unoptimized json parser (#1492) [7d70f02]
* Use vm instead of Function in Node.js (#1509) [75c44d6]

[1.5.5]
* Update Typebot to 2.25.2
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v2.25.2)
* (setVariable) Add Transcription system var (#1507) [40f2120]
* (whatsapp) Enable embed video thumbnail previ… [9b8298b]
* Fix bubble preview image full size [8151d7f]
* (bot) Fix bubble max widths and guest avatar shrinking [3662c0d]
* Convert answerv2 content field to Text field [41ccf24]
* (httpRequests) Fix save variable parsing [304bfcf]
* (transcript) Fix typebot link incorrect next group [fa45564]
* Fix select input displaying id instead of label on refresh [f211a3e]
* (wa) Fix WhatsApp session stuck if state object is empty [8351e20]
* (setVariable) Fix inline code parser [91603aa]

[1.6.0]
* Update Typebot to 2.26.0
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v2.26.0)
* We had to fix an important security issue. It changes how code is executed on Typebot's server. If you have a bot that executes code on the server that contains: `fetch`, `console.log`, `setTimeout` or `setInterval` (It applies only to code that have the Execute on client? option unchecked.). These codes might not work anymore. You should check the new limitations listed here before upgrading: https://docs.typebot.io/editor/blocks/logic/script#limitations-on-scripts-executed-on-server
* Add NocoDB block (#1365) [a17781d]
* Add "Skin Typology" template [5680829]

[1.6.1]
* Update Typebot to 2.26.1
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v2.26.1)
* Fix webhook sample result parsing

[1.7.0]
* Update Typebot to 2.27.0
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v2.27.0)
* Add attachments option to text input (#1608) [6db0464]
* Add "Generate variables" actions in AI blocks [76fcf7e]
* Support Vision for compatible AI models [ee83499]

[1.8.0]
* Update Typebot to 2.28.0
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v2.28.0)

[1.8.1]
* Update Typebot to 2.28.1
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v2.28.1)
* Allow scripts to be executed in WA env [a7c83af]
* (ai) Enable multi urls for vision [3b87801]
* (s3) Skip object removals when S3 not configured [fa14029]
* (openai) Fix custom base url model fetching [5fa946c]
* (forge) Fix incompatible auth schemas when finding fetchers [18c6381]
* (fileUpload) Fix private file upload URLs [2a767e0]

[1.8.2]
* Update Typebot to 2.28.2
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v2.28.2)
* (forge) Fix select inputs [06bf188]

[1.9.0]
* Update Typebot to 3.0.1
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v3.0.1)
* [Beware of breaking changes](https://docs.typebot.io/self-hosting/breaking-changes)
* Add Zendesk block (#1794) [8ced42d]
* (billing) Add reason in sub update metadata [0dbc508]
* Use bun, biome, better ts config, new license and remove all barrel files (#1801) [10750f5]
* (zendesk) Migrate web widget key to block settings [706a095]
* Fix editable overflowing in the variables drawer (#1791) [b03fd77]
* (radar) Fix radar regex bug in prod env [96ed700]
* Fix invalid Google env name in lib [e3aa613]
* Rename Google Sheets API keys in viewer [ba789bb]
* Rename Google API keys for better clarity and granularity across auth, Sheets, and Fonts integrations. [a94f0f7]
* Update DropdownList key prop to use getItemValue [c54e674]
* Fix Google Font bold weight in injectFont function [32690cb]
* Refactor searchRecords to use isNotDefined for valid field check [0d782f7]
* Remove typebot variable reset in resetSessionState [34fe006]
* Update routerProgressBar styles for fixed positioning and z-index adjustments [47fe4e4]
* Re-introduce css imports in _app builder [047d328]
* Fix persistence issue when user remembered [ec24db2]
* Fix scripts exec [12fdfe7]
* (smtp) allow for non-email username [dc870fc]
* Fix variable creation when filtered items not empty [6d350a9]
* Make sure files are not broken in preview if visibility is Private [3f15c26]
* (sendEmail) Fix private attachments not working [19b3148]
* Fix dollar sign prefixed variables in text bubbles [3c07041]
* (rating) Fix icons fill color [d5484f9]
* (setVariable) Fix transcription in loop [c26ab77]
* (number) Accept number with commas on WA [8a28c84]
* Fix blocks search bar not adapting to block labels [445196e]

[1.9.1]
* Update Typebot to 3.1.0
* [Full changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v3.1.0)
* (lp) Update Tailwind CSS configuration to include blog feature files [2834f2e]
* Add script template that reads CSV file and process things [5fb10d2]
* Improve whatsapp runtime logging [7d1c9e6]
* Add Webhook block (#​1815) [59c0ea0]
* Make sure bubble is closing when other chat widget opens up [b31f305]
* Update session handling to continue bot flow when start message is present and no messages to display [5ab08b5]
* Configure bunfig instead of individual npmrc files [d2e4ad8]
* Add npmrc file to authenticate properly for publishing [13c2dd5]
* Add version filed in package json files [698eda7]
* Bump embed libs [a551f77]
* (auth) Attempt to fix issue with link openers in corporate setups (#​1819) [d3a8694]
* Upgrade Stripe API version and dependencies [6ec8f71]

[1.9.2]
* Update typebot-builder to 3.1.1
* [Full Changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v3.1.1)
* (openai) Refine model filtering to exclude audio and realtime models \[[d092d13](https://github.com/baptisteArno/typebot.io/commit/d092d1310f9d3d8cc8c379485885305f76764f98)]
* (anthropic) Add Claude 3.5 Haiku model \[[d4c94ba](https://github.com/baptisteArno/typebot.io/commit/d4c94bafc18b02b5f2d8634a0c907afc96ff5499)]
* (billing) Revert tax ID pre-checkout form \[[e2fc218](https://github.com/baptisteArno/typebot.io/commit/e2fc218f5aef446f57ac69010fc10e4d6d948f88)]
* Introduce global state management for email sending limits \[[933a417](https://github.com/baptisteArno/typebot.io/commit/933a4173995d361cfec12eabe8fdb1d6c774dd1a)]
* Update generateVariable extraction to use nullish instead of optional to not throw on null values \[[345bfac](https://github.com/baptisteArno/typebot.io/commit/345bfac51993071257f7905d31eff55ca612396d)]
* Remove NEXTAUTH_URL_INTERNAL parameter from self-hosting configuration documentation \[[b32ecb1](https://github.com/baptisteArno/typebot.io/commit/b32ecb1dd01b9e6c14a59557ad36227c9980ab6c)]
* (radar) Re-inforce scam keywords detection by parsing set variable blocks value \[[a9b7441](https://github.com/baptisteArno/typebot.io/commit/a9b7441cadc18921fc2a2850a6dbcca0133a6398)]
* Enable SEO for custom domains \[[6b41dcc](https://github.com/baptisteArno/typebot.io/commit/6b41dcc6ff792ac7bc0c7b8781aaf026dadab3f0)]
* Update chat and seat limit functions to prioritize custom limits \[[954151e](https://github.com/baptisteArno/typebot.io/commit/954151e422fff25e5f251e4ab1efc25e87a48675)]

[1.9.3]
* Update typebot-viewer to 3.1.2
* [Full Changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v3.1.2)
* (difyAi) Improve error handling in createChatMessage action \[[a37cdb3](https://github.com/baptisteArno/typebot.io/commit/a37cdb30fe0a7c73e28c869837301f6d4e3791ef)]
* Remove currency parameter from billing API \[[6088a56](https://github.com/baptisteArno/typebot.io/commit/6088a56d3160003d1271780a30c8f622d4d986cb)]
* Add currency selection in precheckout form \[[05cc23e](https://github.com/baptisteArno/typebot.io/commit/05cc23efb1cf0a9549a5c73dad2529e48bd7a264)]
* Change setVariable to accept a list of variables \[[28ebb37](https://github.com/baptisteArno/typebot.io/commit/28ebb374f69291d4706fd003d6a8825298194c47)]
* (whatsapp) Rename 'origin' parameter to 'callFrom' in webhook handlers and related functions \[[4089831](https://github.com/baptisteArno/typebot.io/commit/4089831df038632b4a7e2774ac49b857c47003b7)]
* Update UploadButton to use unique IDs for file input elements \[[4a4e198](https://github.com/baptisteArno/typebot.io/commit/4a4e1982a713074c9c519fbb113400af09441a95)]
* Add onPreviewMessageDismissed prop to Bubble component \[[8016085](https://github.com/baptisteArno/typebot.io/commit/8016085a5a875493452c022110932d796ae270fe)]
* (fileUpload) De-duplicate file names in multi upload \[[5546d11](https://github.com/baptisteArno/typebot.io/commit/5546d112b35cf094b8817ba68de56e4aa208e6a3)]
* Fix markdown escaping for markdown API response format \[[0029f67](https://github.com/baptisteArno/typebot.io/commit/0029f67c20743164ca58c6203b5210cb332d68e5)]
* Add "livechat vs chatbot" blog post ([#&#8203;1882](https://github.com/baptisteArno/typebot.io/issues/1882)) \[[13551a3](https://github.com/baptisteArno/typebot.io/commit/13551a3385a90878342ce91f3de3d3011451f0d8)]
* Add CHAT_API_TIMEOUT parameter to configuration documentation \[[ca1fb59](https://github.com/baptisteArno/typebot.io/commit/ca1fb59e2cfa841d08059bd3644dd35494c8bbe9)]
* Update set variable documentation to include Pop/Shift operations \[[badad22](https://github.com/baptisteArno/typebot.io/commit/badad223f105b01bf4225db3dc12188a88074816)]

[1.10.0]
* Update typebot-builder to 3.2.0
* [Full Changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v3.2.0)
* add "User logged out" telemetry event \[[ddec835](https://github.com/baptisteArno/typebot.io/commit/ddec8351bb9b4ecf5c842d34d7986c0b567b2648)]
* Add limit warning email telemetry events \[[a7a15fd](https://github.com/baptisteArno/typebot.io/commit/a7a15fd4d53bbd5d2a236e55867dce826e7e3fe9)]
* Store existing isolated in global and reuse it ([#&#8203;1914](https://github.com/baptisteArno/typebot.io/issues/1914)) \[[9d62d9d](https://github.com/baptisteArno/typebot.io/commit/9d62d9dbe0be28a5c2b825a261e9bb445ba6fd5a)]
* Add optionnal 'id' argument to target specific instances in embed commands \[[1a1aa3c](https://github.com/baptisteArno/typebot.io/commit/1a1aa3c8249acfa76d6b15bd5c7f55498b1e65b6)]
* (paymentInput) Use stripe-js/pure to lazy load Stripe \[[4dc636d](https://github.com/baptisteArno/typebot.io/commit/4dc636dcc529a951274f231a5028f4ab08de6412)]
* (fileInput) simplify duplicate names logic \[[9d7651d](https://github.com/baptisteArno/typebot.io/commit/9d7651dea391714babbdbc4283b48798d1c0448e)]
* Add explicit react import in emails package to avoid not found bug in CI \[[1d6eca4](https://github.com/baptisteArno/typebot.io/commit/1d6eca4c0c448e59e269f15a925e2b0ec7323585)]
* Attempt to fix "react not found" in ci script \[[c19d208](https://github.com/baptisteArno/typebot.io/commit/c19d208f54f1c50b7f4c99a2bc8c1590f20dede0)]
* Rename updateTypebot script function to updateWorkspace \[[ecb1056](https://github.com/baptisteArno/typebot.io/commit/ecb1056efa809c2ade20375ba5fbea7973b242ca)]
* Fix retro compat and introduce new generateUpload route version \[[c6b09f6](https://github.com/baptisteArno/typebot.io/commit/c6b09f68440018d238372b578a4cfc83dd3fff86)]
* Add blockId prop for file upload to ensure unique URLs \[[d6dc242](https://github.com/baptisteArno/typebot.io/commit/d6dc242906fee20d178fea5470af1393c21207ea)]
* (phoneInput) Fix phone country prefix not changing when already set ([#&#8203;1895](https://github.com/baptisteArno/typebot.io/issues/1895)) \[[676fe94](https://github.com/baptisteArno/typebot.io/commit/676fe940b6a9ed81cd8a93aa3426b4bc3d5852b9)]
* (difyAi) Remove timeout in createChatMessage requests \[[f8642ec](https://github.com/baptisteArno/typebot.io/commit/f8642ec9f38a1e89490ea984cf65f0f662677c4c)]
* Only send limit reached email warning to Free workspaces \[[8779982](https://github.com/baptisteArno/typebot.io/commit/8779982647d1930b7835ecbc7ff4a80ace24dfee)]
* Add 'Meta Workplace Alternatives' blog post ([#&#8203;1908](https://github.com/baptisteArno/typebot.io/issues/1908)) \[[9114ee4](https://github.com/baptisteArno/typebot.io/commit/9114ee465c5d85fee1796016ad770a192e8e6f3a)]
* Add 'best no code website builder' blog post ([#&#8203;1904](https://github.com/baptisteArno/typebot.io/issues/1904)) \[[42a1669](https://github.com/baptisteArno/typebot.io/commit/42a1669c834410758c83cd1dce057f7e3689805e)]
* Add react dependency scripts package json \[[8c5e612](https://github.com/baptisteArno/typebot.io/commit/8c5e6123e84bfdb3bd7bd5d8d53675c633d083c9)]

[1.10.1]
* CLOUDRON_OIDC_PROVIDER_NAME implemented

[1.11.0]
* Update typebot-builder to 3.3.0
* [Full Changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v3.3.0)
* (conditions) Add >= and <= comparison operators ([#&#8203;1938](https://github.com/baptisteArno/typebot.io/issues/1938)) \[[b2865c6](https://github.com/baptisteArno/typebot.io/commit/b2865c61527f5e10671d1a213e14b2cfa4ed5548)]
* (buttons) filter unique items from dynamic buttons variable ([#&#8203;1926](https://github.com/baptisteArno/typebot.io/issues/1926)) \[[b409c96](https://github.com/baptisteArno/typebot.io/commit/b409c962bed419b2c4784696879acde086aeaf4d)]
* Add distinctId to groupIdentify methods \[[5233c79](https://github.com/baptisteArno/typebot.io/commit/5233c794e0bc88d6594c5f6a28751a8ea0160702)]
* Add Pi theme in gallery \[[8518a52](https://github.com/baptisteArno/typebot.io/commit/8518a527871836eedec66679873ed0896d6e159e)]

[1.12.0]
* Update typebot-builder to 3.4.2
* [Full Changelog](https://github.com/baptisteArno/typebot.io/releases/tag/v3.4.2)
* Fix forge block select fetch not working \[[9918201](https://github.com/baptisteArno/typebot.io/commit/9918201d66151ab768a6fbd37ac491353fe57697)]
* Add credentials documentation \[[c0954da](https://github.com/baptisteArno/typebot.io/commit/c0954dad34925c39951ed87254102a9445cfd809)]
* Add delete account documentation \[[ab556cd](https://github.com/baptisteArno/typebot.io/commit/ab556cd7af37873975f7a702f4fdc8f323dee8ba)]
* Revert invalid docker deploy workflow and fix the artifacts download merging \[[3080695](https://github.com/baptisteArno/typebot.io/commit/3080695c09c1d19e0da185f9c31fecde955e61d4)]
* (anthropic) Fix ignored system message \[[322bd07](https://github.com/baptisteArno/typebot.io/commit/322bd07f9e7362276e85aa855716b07a81cdeacd)]
* Add custom model and prompt for group title gen \[[547fa92](https://github.com/baptisteArno/typebot.io/commit/547fa92e79b708afcc1db777254549164d5e7cb7)]
* Fix User logged in track event \[[362bdc8](https://github.com/baptisteArno/typebot.io/commit/362bdc855e6e904ef24aaf48bcedd1a6398a9bcc)]
* Fix prevPlan retrieve logic in Stripe webhooks \[[09bbe1d](https://github.com/baptisteArno/typebot.io/commit/09bbe1df4167b34a7865be01e4fffffc7d322a91)]
* Add error variable saving for Set Variable block with code expression \[[8061585](https://github.com/baptisteArno/typebot.io/commit/806158541fbea22070aa30fa6b42a1498516aefd)]
* Add buttons input layout customization option \[[5198800](https://github.com/baptisteArno/typebot.io/commit/5198800c8589d4910b2f4007bf2102d90407405a)]
* Add option to customize system messages \[[789b38e](https://github.com/baptisteArno/typebot.io/commit/789b38ea3f510ccdf216011eda679389117ced16)]
* Add "Additional Instructions" field to "Ask assistant" action \[[14581e3](https://github.com/baptisteArno/typebot.io/commit/14581e3ec2649f6700c303405bf0b99f4ddde2df)]

