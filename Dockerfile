# ARG needs to be defined prior the first FROM statement if used in any FROM statement
# renovate: datasource=github-releases depName=baptisteArno/typebot.io versioning=semver extractVersion=^v(?<version>.+)$
ARG VERSION=3.4.2

FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4 AS base

FROM baptistearno/typebot-builder:${VERSION} AS typebot-builder
FROM baptistearno/typebot-viewer:${VERSION} AS typebot-viewer

FROM base AS runner

ARG NODE_VERSION=22.13.1
RUN mkdir -p /usr/local/node-${NODE_VERSION} && curl -L https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-${NODE_VERSION}
ENV PATH /usr/local/node-${NODE_VERSION}/bin:$PATH

ENV NODE_ENV production
WORKDIR /app/code

RUN npm --global install pnpm

RUN pnpm install next-runtime-env prisma

COPY --from=typebot-builder ./app ./builder/
COPY --from=typebot-viewer ./app ./viewer/

COPY supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/typebot/supervisord.log /var/log/supervisor/supervisord.log

RUN rm -rf /app/code/builder/apps/builder/public/__ENV.js && ln -s /run/builder__ENV.js /app/code/builder/apps/builder/public/__ENV.js
RUN rm -rf /app/code/viewer/apps/viewer/public/__ENV.js && ln -s /run/viewer__ENV.js /app/code/viewer/apps/viewer/public/__ENV.js

# generate those here, since otherwise they will be generated on runtime which fails due to read-only filesystem
RUN /app/code/builder/node_modules/.bin/prisma generate --schema=/app/code/builder/packages/prisma/postgresql/schema.prisma
RUN /app/code/viewer/node_modules/.bin/prisma generate --schema=/app/code/viewer/packages/prisma/postgresql/schema.prisma
# https://github.com/vercel/next.js/issues/10111
RUN ln -s /run/typebot/nextcache /app/code/builder/apps/builder/.next/cache

COPY start.sh /app/pkg/

RUN chmod +x /app/pkg/start.sh

CMD [ "/app/pkg/start.sh" ]
