#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, Origin, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.EMAIL || !process.env.PASSWORD) {
    console.log('EMAIL, PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const BOT_NAME = 'test typebot ' + Math.floor((Math.random() * 100) + 1);

    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 50000;

    let browser, app, shareLink;
    const username = process.env.EMAIL;
    const password = process.env.PASSWORD;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TIMEOUT);
    }

    function sleep(millis) {
        return new Promise(resolve => setTimeout(resolve, millis));
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    async function loginOIDC(username, password, alreadyAuthenticated = true) {
        await browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/signin`);

        await waitForElement(By.xpath('//button[contains(text(), "Continue with")]'))
        await browser.findElement(By.xpath('//button[contains(text(), "Continue with")]')).click();

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }

        await browser.wait(until.elementLocated(By.xpath('//p[contains(text(), "\'s workspace")]')), TIMEOUT);
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}`);

        // UI is a bit shakey
        await browser.sleep(2000);

        await waitForElement(By.xpath('//p[contains(text(), "\'s workspace")]'));
        await browser.findElement(By.xpath('//p[contains(text(), "\'s workspace")]/ancestor::button')).click();

        await browser.sleep(1000);

        await waitForElement(By.xpath('//span[text()="Log out"]'));
        await browser.findElement(By.xpath('//span[text()="Log out"]/ancestor::button')).click();

        await waitForElement(By.xpath('//button[contains(text(), "Continue with")]'))
    }

    async function createBot() {
        await browser.get(`https://${app.fqdn}/typebots/create`);

        await waitForElement(By.xpath('//button[contains(text(), "Start from a template")]'));
        await browser.findElement(By.xpath('//button[contains(text(), "Start from a template")]')).click();

        await waitForElement(By.xpath('//p[contains(text(), "Lead Generation")]/ancestor::button'));
        await browser.findElement(By.xpath('//p[contains(text(), "Lead Generation")]/ancestor::button')).click();

        await waitForElement(By.xpath('//button[contains(text(), "Use this template")]'));
        await browser.findElement(By.xpath('//button[contains(text(), "Use this template")]')).click();

        // dismiss Language popup
        if (app.manifest.upstreamVersion === '2.26.1') {
            await waitForElement(By.xpath('//h2[contains(text(), "Language")]'));
            await browser.findElement(By.xpath('//section/button[@aria-label="Close"]')).click();
        }

        //rename
        await waitForElement(By.xpath('//div[./span[text()="Lead Generation"]]'));
        await browser.findElement(By.xpath('//div[./span[text()="Lead Generation"]]')).click();

        await waitForElement(By.xpath('//input[@value="Lead Generation"]'));
        let title_field = browser.findElement(By.xpath('//input[@value="Lead Generation"]'));
        await title_field.click();
        await title_field.sendKeys(Key.CONTROL + 'a' + Key.COMMAND + 'a' + Key.BACK_SPACE);
        await browser.sleep(2000);
        await title_field.sendKeys(`${BOT_NAME}`);
        await title_field.sendKeys(Key.RETURN);
        await browser.sleep(2000);

        await browser.findElement(By.xpath('//button[text()="Publish"]')).click();

        await browser.sleep(2000);
    }

    async function checkBot() {
        await browser.get(`https://${app.fqdn}/typebots`);

        await waitForElement(By.xpath(`//p[text()="${BOT_NAME}"]`));
        await browser.findElement(By.xpath(`//p[text()="${BOT_NAME}"]`)).click();

        // what to check here?
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', async function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);     });

    it('can get app information', getAppInfo);
    it('can login OIDC', loginOIDC.bind(null, username, password, false));
    it('can create bot', createBot);
    it('check bot', checkBot);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });

    it('can login OIDC', loginOIDC.bind(null, username, password));
    it('check bot', checkBot);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);

        // wait when all services are up and running
        await sleep(20000);
    });

    it('can login OIDC', loginOIDC.bind(null, username, password));
    it('check bot', checkBot);
    it('can logout', logout);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });
    it('can get app information', getAppInfo);

    // needs more time for some reason
    it('sleep', async function() { await browser.sleep(5000); });

    it('can login OIDC', loginOIDC.bind(null, username, password));
    it('check bot', checkBot);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app for update', async function () { execSync(`cloudron install --appstore-id io.typebot.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can login OIDC', loginOIDC.bind(null, username, password));
    it('can create bot', createBot);
    it('check bot', checkBot);
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });

    it('can login OIDC', loginOIDC.bind(null, username, password));
    it('check bot', checkBot);
    it('can logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});

