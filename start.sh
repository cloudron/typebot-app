#!/bin/bash

set -eu

echo "==> Creating directories"
mkdir -p /run/typebot/nextcache

if [[ ! -f /app/data/.encryption_secret ]]; then
    echo "=> First run"
    openssl rand -base64 24 > /app/data/.encryption_secret
fi
export ENCRYPTION_SECRET=$(cat /app/data/.encryption_secret)

if [[ ! -f /app/data/env.sh ]];then
cat > /app/data/env.sh << EOF
# Add custom ENV configuration in this file
# https://docs.typebot.io/self-hosting/configuration
EOF
fi

export DEFAULT_WORKSPACE_PLAN=UNLIMITED
export DISABLE_SIGNUP=false # without this even oidc user cannot login

# environment variables that cannot be overriden
export DATABASE_URL="$CLOUDRON_POSTGRESQL_URL"
export ENCRYPTION_SECRET="$ENCRYPTION_SECRET"
export NEXTAUTH_URL="$CLOUDRON_APP_ORIGIN"
export NEXT_PUBLIC_VIEWER_URL="https://$TYPEBOT_VIEWER_HOST"
export NEXT_PUBLIC_SMTP_FROM="${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Typebot Notifications} <${CLOUDRON_MAIL_FROM}>"

export SMTP_USERNAME="$CLOUDRON_MAIL_SMTP_USERNAME"
export SMTP_PASSWORD="$CLOUDRON_MAIL_SMTP_PASSWORD"
export SMTP_HOST="$CLOUDRON_MAIL_SMTP_SERVER"
export SMTP_PORT="$CLOUDRON_MAIL_SMTP_PORT"
export SMTP_AUTH_DISABLED=true

# Cloudron OIDC
if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    export CUSTOM_OAUTH_NAME="${CLOUDRON_OIDC_PROVIDER_NAME:-Cloudron}"
    export CUSTOM_OAUTH_CLIENT_ID="$CLOUDRON_OIDC_CLIENT_ID"
    export CUSTOM_OAUTH_CLIENT_SECRET="$CLOUDRON_OIDC_CLIENT_SECRET"
    export CUSTOM_OAUTH_WELL_KNOWN_URL="$CLOUDRON_OIDC_DISCOVERY_URL"
    export CUSTOM_OAUTH_SCOPE="openid profile email"
    export CUSTOM_OAUTH_USER_ID_PATH="sub"
    export CUSTOM_OAUTH_USER_EMAIL_PATH="email"
    export CUSTOM_OAUTH_USER_NAME_PATH="name"
fi

# source this later to allow users to put in their own email/smtp auth
source /app/data/env.sh

echo 'Injecting environment variables into frontend...'
cd /app/code/builder/apps/builder;
node  -e "const { configureRuntimeEnv } = require('next-runtime-env/build/configure'); configureRuntimeEnv();"
cd /app/code/viewer/apps/viewer;
node  -e "const { configureRuntimeEnv } = require('next-runtime-env/build/configure'); configureRuntimeEnv();"
cd /app/code

echo "==> Database migration"
/app/code/builder/node_modules/.bin/prisma migrate deploy --schema=builder/packages/prisma/postgresql/schema.prisma

echo "==> Changing ownership"
chown -R cloudron:cloudron /app/data /run/typebot

echo "==> Starting Typebot"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Typebot
